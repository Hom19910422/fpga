
/* Debounce_4Bit程式 */
/* 4Bit的Debounce */
module KEY_Debounce_4Bit( CLK, RST, KEY_In, KEY_Out );
	input  CLK, RST;
	input  [3:0] KEY_In;
	output [3:0] KEY_Out;

	KEY_Debounce KEY_DB0(
		.CLK( CLK ),
		.RST( RST ),
		.KEY_In( KEY_In[0] ),
		.KEY_Out( KEY_Out[0] )
	);
	KEY_Debounce KEY_DB1(
		.CLK( CLK ),
		.RST( RST ),
		.KEY_In( KEY_In[1] ),
		.KEY_Out( KEY_Out[1] )
	);
	KEY_Debounce KEY_DB2(
		.CLK( CLK ),
		.RST( RST ),
		.KEY_In( KEY_In[2] ),
		.KEY_Out( KEY_Out[2] )
	);
	KEY_Debounce KEY_DB3(
		.CLK( CLK ),
		.RST( RST ),
		.KEY_In( KEY_In[3] ),
		.KEY_Out( KEY_Out[3] )
	);

endmodule
