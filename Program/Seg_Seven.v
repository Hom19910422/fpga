
/* 七段顯示器解碼(共陽) */
module Seg_Seven( SEL_In, DIG_In, SEL_Out, DIG_Out );
	input [2:0] SEL_In;		// Use PNP
	input [3:0] DIG_In;		//	MSB D, C, B, A LSB
	output [5:0] SEL_Out;	// 七段顯示器選擇
	output [7:0] DIG_Out;	// MSB dp, g, f, e, d, c, b, a LSB

	reg [5:0] rSEL_Out;
	reg [7:0] rSEG_Out;

	always @( SEL_In ) begin
		case( SEL_In )
			3'b000:	rSEL_Out = 6'b111110;	// 1
			3'b001:	rSEL_Out = 6'b111101;	// 2
			3'b010:	rSEL_Out = 6'b111011;	// 3
			3'b011:	rSEL_Out = 6'b110111;	// 4
			3'b100:	rSEL_Out = 6'b101111;	// 5
			3'b101:	rSEL_Out = 6'b011111;	// 6
			3'b111:	rSEL_Out = 6'b000000;	// all on
			default:	rSEL_Out = 6'b111111;	// all off
		endcase
	end

	always @( DIG_In ) begin
		case( DIG_In )
			4'b0000:	rSEG_Out = 8'b11000000;	// 0
			4'b0001:	rSEG_Out = 8'b11111001;	// 1
			4'b0010:	rSEG_Out = 8'b10100100;	// 2
			4'b0011:	rSEG_Out = 8'b10110000;	// 3
			4'b0100:	rSEG_Out = 8'b10011001;	// 4
			4'b0101:	rSEG_Out = 8'b10010010;	// 5
			4'b0110:	rSEG_Out = 8'b10000011;	// 6
			4'b0111:	rSEG_Out = 8'b11111000;	// 7
			4'b1000:	rSEG_Out = 8'b10000000;	// 8
			4'b1001:	rSEG_Out = 8'b10010000;	// 9
			4'b1010:	rSEG_Out = 8'b10100111;	// 
			4'b1011:	rSEG_Out = 8'b10110011;	// 
			4'b1100:	rSEG_Out = 8'b10011101;	// 
			4'b1101:	rSEG_Out = 8'b10010110;	// 
			4'b1110:	rSEG_Out = 8'b10000111;	// 
			4'b1111:	rSEG_Out = 8'b11111111;	// off
			default:	rSEG_Out = 8'b01111111;	// dp
		endcase
	end

	assign SEL_Out = rSEL_Out;
	assign DIG_Out = rSEG_Out;

endmodule
