
/* Debounce程式 */
/* 以取樣訊號DeB_Num次來判斷是否穩定 */
module KEY_Debounce( CLK, RST, KEY_In, KEY_Out );
	input  CLK, RST;
	input  KEY_In;
	output KEY_Out;

	reg rKEY_Out = 1;
	reg [DeB_Num-1:0] Bounce = DeB_RST;	// 初始化

	parameter DeB_Num = 4;			// 取樣次數
	parameter DeB_SET = 4'b0000;	// 設置
	parameter DeB_RST = 4'b1111;	// 重置

	always @( posedge CLK or negedge RST ) begin	//	一次約200Hz 5ms
		integer i;
		if( !RST )
			Bounce <= DeB_RST;	// Bounce重置
		else begin	// 取樣4次
			Bounce[0] <= KEY_In;
			for( i=0; i<DeB_Num-1; i=i+1 )
				Bounce[i+1] <= Bounce[i];
		end
	end

	always @( Bounce ) begin
		if( Bounce == DeB_RST )
			rKEY_Out = 1;
		else if( Bounce == DeB_SET )
			rKEY_Out = 0;
	end

	assign KEY_Out = rKEY_Out;

endmodule
