
/* 七段顯示器顯示(共陽) (自動掃描顯示) */
module Digit_Seven( CLK, RST, DIG_In, SEL_Out, DIG_Out );
	input CLK, RST;
	input  [20:0] DIG_In;
	output [5:0] SEL_Out;
	output [7:0] DIG_Out;
	
	reg [2:0] rAddress;
	wire [2:0] Address;
	wire [31:0] Wire_ND;
	
	Number_Digit ND(	// 進位轉換
		.CLK(CLK),
		.RST(RST),
		.Address(Address),
		.DIG_In(DIG_In),
		.DIG_Out(Wire_ND)
	);
	Seg_Seven SS(	// 顯示數字
		.SEL_In(Address),
		.DIG_In(Wire_ND),
		.SEL_Out(SEL_Out),
		.DIG_Out(DIG_Out)
	);

	always @( posedge CLK or negedge RST ) begin
		if( !RST )
			rAddress = 1'b0;
		else if( Address == 3'b101 )
			rAddress = 1'b0;
		else
			rAddress = rAddress + 1'b1;
	end
	
	assign Address = rAddress;

endmodule
