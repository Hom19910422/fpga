
/* 除頻器 Use 50MHz OSC */
module Freq_Divider( CLK, RST, CLK_Out );

	input  CLK, RST;
	output CLK_Out;

	reg rCLK_Out;
	reg [DivW-1:0] CLK_Cnt = 0;

	// 除頻設定   1Hz 1s
	parameter Div  = 26'd50_000_000;	// 除頻數(Even)
	parameter Div2 = 26'd25_000_000;	// Div/2
	parameter DivW = 26;					// Divide寬度

	/*// 除頻設定   2Hz 500ms
	parameter Div  = 23'd25_000_000;	// 除頻數(Even)
	parameter Div2 = 23'd12_500_000;	// Div/2
	parameter DivW = 23;					// Divide寬度
	*/
	/*// 除頻設定  50Hz 20ms
	parameter Div  = 20'd1_000_000;	// 除頻數(Even)
	parameter Div2 = 20'd500_000;		// Div/2
	parameter DivW = 20;					// Divide寬度
	*/
	/*// 除頻設定 100Hz 10ms
	parameter Div  = 19'd500_000;		// 除頻數(Even)
	parameter Div2 = 19'd250_000;		// Div/2
	parameter DivW = 19;					// Divide寬度
	*/
	/*// 除頻設定 200Hz 5ms
	parameter Div  = 18'd250_000;		// 除頻數(Even)
	parameter Div2 = 18'd125_000;		// Div/2
	parameter DivW = 18;					// Divide寬度
	*/
	/*// 除頻設定  1kHz 1ms
	parameter Div  = 16'd250_000;		// 除頻數(Even)
	parameter Div2 = 16'd125_000;		// Div/2
	parameter DivW = 16;					// Divide寬度
	*/

	always @( posedge CLK or negedge RST ) begin
		if( !RST )
			CLK_Cnt <= 0;
		else if( CLK_Cnt == Div-1 )
			CLK_Cnt <= 0;
		else
			CLK_Cnt <= CLK_Cnt + 1'b1;
	end

	always @( posedge CLK or negedge RST ) begin
		if( !RST )
			rCLK_Out <= 0;
		else if( CLK_Cnt <= Div2-1 )
			rCLK_Out = 0;
		else
			rCLK_Out = 1'b1;
	end

	assign CLK_Out = rCLK_Out;

endmodule
