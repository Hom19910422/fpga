
/* 進位轉換 */
module Num_Digit( CLK, RST, Address, DIG_In, DIG_Out );
	input CLK, RST;
	input [2:0] Address;
	input [20:0] DIG_In;
	output [31:0] DIG_Out;

	reg [31:0] rDIG_Out [5:0];

	always @( posedge CLK or negedge RST ) begin
		if(!RST)
			rDIG_Out[Address] = 32'd0;
		else begin
			rDIG_Out[0] = ( DIG_In        )%10;
			rDIG_Out[1] = ( DIG_In/10     )%10;
			rDIG_Out[2] = ( DIG_In/100    )%10;
			rDIG_Out[3] = ( DIG_In/1000   )%10;
			rDIG_Out[4] = ( DIG_In/10000  )%10;
			rDIG_Out[5] = ( DIG_In/100000 )%10;
		end
	end

	assign DIG_Out = rDIG_Out[Address];

endmodule
