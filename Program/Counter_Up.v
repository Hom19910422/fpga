
/* 上數計數器 */
module Counter_Up( CLK, RST, Cnt );
	input CLK, RST;
	output [Cnt_SB-1:0] Cnt;

	reg [Cnt_SB-1:0] rCnt = Cnt_Min;

	/* 計數資訊 */
	parameter Cnt_UP  = 1'b1;			// 計數值
	parameter Cnt_SB  = 20;				// 計數寬度
	parameter Cnt_Min = 20'd0;			// 計數最小值
	parameter Cnt_Max = 20'd999999;	// 計數最大值

	always @( posedge CLK or negedge RST ) begin
		if( !RST )
			rCnt <= Cnt_Min;
		else if( rCnt == Cnt_Max )
			rCnt <= Cnt_Min;
		else
			rCnt <= rCnt + Cnt_UP;
	end

	assign Cnt = rCnt;

endmodule
